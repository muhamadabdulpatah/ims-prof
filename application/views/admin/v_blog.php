<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Isi Blog</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Judul</th>
                    <th>Content</th>
                    <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach($tampil->result() as $t){?>
                  <tr>
                    <td><img src="<?php echo base_url();?>images/blog/<?php echo $t->gambar_blog;?>" width="100" height = "100" ></td>                    
                    <td><?= $t->judul_blog;?></td>
                    <td><?= $t->deskripsi_blog;?></td>
                    <td><a href="<?= site_url()?>/blog/edit/<?= $t->id_blog ?>" class="btn btn-primary"><i class="fa fa-edit" ></i> Update</a> |
                        <a href="<?= site_url()?>/blog/delete/<?= $t->id_blog ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                    </td>
                  <?php };?>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>