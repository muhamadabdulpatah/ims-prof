<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <?= form_open_multipart('blog/update');?>
      <div class="box box-primary">
        <div class="box-header">
          <button class="btn btn-primary" type="submit">Update</button>
        </div>
        <?php
          foreach ($blog as $a) {
           
          
        ?>
        <div class="box-body">
            <div class="form-group">
              <label for="">Judul</label>
              <input type="hidden" name="id" id="id" value="<?php echo $a->id_blog; ?>">
              <input type="text" class="form-control" name="judul" id="judul" value="<?php echo $a->judul_blog; ?>">
            </div>
            <div class="form-group">
              <label for="">Gambar</label>
              <input type="file" class="form-control" name="gambar" id="gambar">
            </div>
        </div>
      </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Content
          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip"
            title="Remove">
            <i class="fa fa-times"></i></button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body pad">
            <textarea name="desc" class="textarea" placeholder="Place some text here"
            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
              <?php echo $a->deskripsi_blog ?>
            </textarea>
        </div>
      </div>
      <?php
        }
      ?>
      <?= form_close();?>
    </div>
  </div>
</section>