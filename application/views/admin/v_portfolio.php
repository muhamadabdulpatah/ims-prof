<section class="content">
      <div class="row">
        <div class="flash-data" data-flashdata="<?php echo $this->session->flashdata('message');?>">
        </div>
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                <span class="fa fa-plus"> Tambah Data</span>
              </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Project Name</th>
                    <th>Project Type</th>
                    <th>Profil Image</th>
                    <th>Description</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <tbody>     
                  <?php
                  $no = 1;
                  foreach ($test as $a) {

                    ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $a->nama_project;?></td>
                      <td><?php echo $a->type_project; ?></td>
                      <td><img src="<?php echo base_url();?>images/<?php echo $a->foto;?>" width="100" height = "100" ></td>
                      <td><?php echo $a->deskripsi; ?></td>
                      <td><a 
                        href="javascript:;"
                        data-id="<?php echo $a->id_portfolio; ?>"
                        data-foto="<?php echo $a->foto; ?>"
                        data-deskripsi="<?php echo $a->deskripsi; ?>"
                        data-toggle="modal" data-target="#edit-data">
                        <button data-toggle="modal" data-target="#ubah-data" class="btn btn-info fa fa-pencil"> Update</button>
                      </a> |
                      <a id="tombol-hapus" href="<?php echo site_url();?>/portfolio/delete/<?php echo $a->id_portfolio?>" class="btn btn-danger tombol-hapus"><i class="fa fa-trash"></i> Delete</a>
                    </td>
                    <?php
                  }
                  ?>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Project Name</th>
                  <th>Project Type</th>
                  <th>Profil Image</th>
                  <th>Description</th>
                  <th>Option</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Portfolio</h4>
            </div>
            <?php echo form_open_multipart('portfolio/saveGuru');?>
            <div class="modal-body">
              <div class="box-body">
                <div class="form-group">
                  <label for="">Project Name</label>
                  <input type="text" class="form-control" name="project_name">
                </div>
                <div class="form-group">
                  <label for="">Project Type</label>
                  <select name="type" id="type" class="form-control">
                    <option value="#" selected="" disabled="">--Please Choose--</option>
                    <option value="Mobile App">Mobile App</option>
                    <option value="Web App">Web App</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Upload Image</label>
                  <input type="file" name="foto">
                </div>
                <div class="form-group">
                  <label for="">Description</label>
                  <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" class="form-control">

                  </textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?php echo form_close();?>
        </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>      
      <!-- /.row -->
    <div class="modal fade" id="edit-data">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Portfolio</h4>
            </div>
            <?php echo form_open_multipart('portfolio/update');?>
            <div class="modal-body">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputFile">Upload Image</label>
                  <input type="file" name="foto" id="foto">
                  <input type="hidden" name="id" id="id">
                </div>
                <div class="form-group">
                  <label for="">Description</label>
                  <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" class="form-control">

                  </textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?php echo form_close();?>
        </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
</section>