<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="UTF-8">
	<title>Flat HTML5/CSS3 Login Form</title>
	<link rel="stylesheet" href="<?= base_url();?>/assets/login/style.css">
</head>
<body>
<!-- partial:index.partial.html -->
	<div class="login-page">
		<div class="form">
			<form class="register-form" action="">
				<input type="text" placeholder="name"/>
				<input type="password" placeholder="password"/>
				<input type="text" placeholder="email address"/>
				<button>create</button>
				<p class="message">Already registered? <a href="#">Sign In</a></p>
			</form>
			<form class="login-form" action="<?= base_url('')?>login/aksi_login" method="post">
				<input type="text" placeholder="username" name="username" id="username"/>
				<input type="password" placeholder="password" name="password" id="password" />
				<button>login</button>
					<p class="message">Not registered? <a href="#">Create an account</a></p>
			</form>
		</div>
	</div>
	<!-- partial -->
		<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
		<script  src="<?=base_url();?>/assets/login/script.js"></script>
	</body>
</html>