
<?php 

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();		
        $this->load->model('m_login');
        // $this->load->library('session'); 

	}

	function index(){
		$this->load->view('admin/v_login');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			);
		$cek = $this->m_login->cek_login("users",$where)->num_rows();
		if($cek > 0){

			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);

			$this->session->set_userdata($data_session);

			redirect(site_url("/welcome"));

		}else{
			$this->session->set_flashdata('message','password atau username yang anda masukan salah');
			redirect(site_url('/login'));
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(site_url('/login'));
	}
}
