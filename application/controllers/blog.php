<?php
    class blog extends CI_Controller
    {
        function __construct()
        {
		parent::__construct();
        $this->load->model('m_blog');
        $this->load->library('session');
        if($this->session->userdata('status') != "login"){
            redirect(site_url("/login"));
        }
        }
        
        public function index()
        {
        $data['tampil'] = $this->m_blog->getALL();
        $data['page'] = 'Blog';
		$data['content'] = 'admin/v_blog';
		$data['title'] = 'Blog';
		$this->load->view('index',$data);
        }

        public function list_add()
        {
        $data['page'] = 'Blog';
		$data['content'] = 'admin/v_blog_list';
		$data['title'] = 'Blog';
		$this->load->view('index',$data);
        }
        
        function save()
        {
        $judul= $this->input->post('judul');
        $desc=$this->input->post('desc');
		$foto = $_FILES['gambar'];
		if ($foto='') {
			
		}else{
			$config['upload_path']	= './images/blog';
			$config['allowed_types'] = 'jpg|png|gif';

			$this->load->library('upload',$config);
			if (!$this->upload->do_upload('gambar')) {
				echo "Upload Gagal";die();
			}else{
				$foto=$this->upload->data('file_name');
			}
		}
		$data = array(
            'judul_blog'        => $judul,
            'deskripsi_blog'    => $desc,
			'gambar_blog'	    => $foto
		);
		$this->m_blog->save($data);
		redirect('blog');
        }
        function edit($id){
            $data['page'] = 'Blog';
            $data['content'] = 'admin/v_edit_blog';
            $data['title'] = 'Blog';
            $where = array('id_blog' => $id);
            $data['blog'] = $this->m_blog->edit_data($where,'blog')->result();
            $this->load->view('index',$data);
        }
        function update()
        {
            $id = $this->input->post('id');
            //$id = $this->uri->segment('3');
            $judul= $this->input->post('judul');
            $desc=$this->input->post('desc');
            $foto = $_FILES['gambar'];
            if ($foto='') {
            
            }else{
                $config['upload_path']  = './images/blog';
                $config['allowed_types'] = 'jpg|png|gif';

                $this->load->library('upload',$config);
                if (!$this->upload->do_upload('gambar')) {
                    echo "Upload Gagal";die();
                }else{
                    $foto=$this->upload->data('file_name');
                }
            }
            $data = array(
                'judul_blog'        => $judul,
                'deskripsi_blog'    => $desc,
                'gambar_blog'       => $foto
            );
            $this->m_blog->update($data,$id);
            // $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil disimpan didatabase.</div>');
            redirect('blog');
        }
        function delete($id)
        {
            // $this->load->model('m_blog');
            $this->m_blog->delete($id);
            redirect('blog');
        }
    }
    
?>