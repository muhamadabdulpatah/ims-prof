<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_portfolio');
		if($this->session->userdata('status') != "login"){
			redirect(site_url("/login"));
		}
	}

	public function index()
	{
		$data['test'] = $this->m_portfolio->GetAll()->result();
		$data['page'] = 'Dashboard';
		$data['content'] = 'admin/v_portfolio';
		$data['title'] = 'Dashboard';
		$this->load->view('index',$data);
	}
	function saveGuru(){
                
		$nip=$this->input->post('deskripsi');
		$nama = $this->input->post('project_name');
		$type = $this->input->post('type');
		$foto = $_FILES['foto'];
		if ($foto='') {
			
		}else{
			$config['upload_path']	= './images';
			$config['allowed_types'] = 'jpg|png|gif';
			$config['max_size'] = '4096';

			$this->load->library('upload',$config);
			if (!$this->upload->do_upload('foto')) {
				echo "Upload Gagal";die();
			}else{
				$foto=$this->upload->data('file_name');
			}
		}
		$data = array(
			'deskripsi' => $nip,
			'nama_project' => $nama,
			'type_project' => $type,
			'foto'		=> $foto
		);
		$this->m_portfolio->save($data);
		$this->session->set_flashdata('message','Data telah di tambahkan');
		redirect('portfolio');
	}
	function update()
	{
		$id = $this->input->post('id');
		$nip=$this->input->post('deskripsi');
		$foto = $_FILES['foto'];
		if ($foto='') {
			
		}else{
			$config['upload_path']	= './images';
			$config['allowed_types'] = 'jpg|png|gif';
			$config['max_size'] = '4096';

			$this->load->library('upload',$config);
			if (!$this->upload->do_upload('foto')) {
				echo "Upload Gagal";die();
			}else{
				$foto=$this->upload->data('file_name');
			}
		}
		$data = array(
			'deskripsi' => $nip,
			'foto'		=> $foto
		);
		$this->m_portfolio->update($data,$id);
		redirect('portfolio');
	}
	function delete()
	{
		$id = $this->uri->segment(3);
		$this->m_portfolio->hapus($id);
		$this->session->set_flashdata('message','Data di hapus');
		redirect('portfolio');
	}

}
