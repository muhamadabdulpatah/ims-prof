<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_blog');
		if($this->session->userdata('status') != "login"){
			redirect(site_url("/login"));
		}
	}
	public function index()
	{
		$data['blog'] = $this->m_blog->CountBlog();
		$data['portfolio'] = $this->m_blog->CountPortfolio();
		$data['page'] = 'Dashboard';
		$data['content'] = 'admin/content';
		$data['title'] = 'Dashboard';
		$this->load->view('index',$data);
	}
}
