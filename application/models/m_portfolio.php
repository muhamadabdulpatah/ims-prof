<?php

class M_Portfolio extends CI_Model
{
	
	function GetAll()
	{
		return $this->db->get('portfolio');

	}
	function save($data)
	{
		$this->db->insert('portfolio',$data);
	}
	function update($data,$id)
	{
		$this->db->where('id_portfolio',$id);
		$this->db->update('portfolio',$data);
	}
	function hapus($id)
	{
		$this->db->where('id_portfolio',$id);
		$this->db->delete('portfolio');
	}
}