<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_blog extends CI_Model
{
	public function getALL()
	{
		$query = $this->db->get('blog');
		return $query;
    }
    
    public function save($data)
    {
        $this->db->insert('blog',$data);
    }
    function edit_data($where,$table){      
        return $this->db->get_where($table,$where);
    }
    function update($data,$id)
    {
        $this->db->where('id_blog',$id);
        $this->db->update('blog',$data);
    }
    public function delete($id)
    {
        return $this->db->delete('blog', array("id_blog" => $id));
    }
    public function CountBlog()
    {   
        $query = $this->db->get('blog');
        if($query->num_rows()>0)
        {
            return $query->num_rows();
        }
        else
        {
            return 0;
        }
    }
    public function CountPortfolio()
    {   
        $query = $this->db->get('portfolio');
        if($query->num_rows()>0)
        {
            return $query->num_rows();
        }
        else
        {
            return 0;
        }
    }
}