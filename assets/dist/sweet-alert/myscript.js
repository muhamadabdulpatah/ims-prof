const flashData = $('.flash-data').data('flashdata');
if (flashData) {
	Swal.fire({
		title: 'Data Portfolio',
		text : flashData,
		type : 'success'
	});
}

//tombol hapus

$('#tombol-hapus').on('click', function(e){

	e.preventDefault();
	const href = $(this).attr('href');
	Swal.fire({
		title: 'Apakah anda yakin ?',
		text: "Data Portfolio Akan Di Hapus",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Hapus Data!'
	}).then((result) => {
		if (result.value) {
			document.location.href = href;
		}
	})

});

//gagal login
const Gagal = $('.gagal-login').data('flashdata');
if (Gagal) {
	Swal.fire({
  		type: 'error',
  		title: 'Oops...',
  		text: Gagal
	});
}